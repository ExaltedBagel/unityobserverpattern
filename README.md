# Unity Scriptable Objects (Observer Pattern)

## Summary
This project aims at showing the potential of Scriptable Objects as a tool for extensible game architecture. This project builds on the basis shown in Unity's [Three ways to architect your game with ScriptableObjects](https://unity.com/how-to/architect-game-code-scriptable-objects). If you are not familiar with this, you should go read it before proceeding, as I will expand on the concept. 

The project explores a simple use-case: A player taking damage. When a player takes damage, the following must happen:
1. The damage is substracted from the player HP.
2. A health bar is updated with the new player HP total.
3. A sound is played.
4. The player flashes red.
5. If the HP total is under 0, the player dies.

## Code Structure
The code structure is the following

### Utils
Contains a utility class to allow marking a field as *ReadOnly* to display the value in the inspector, but prevent modifying it.

### Visuals
Contains the components necessary to interpolate a material or text color in a *Coroutine*. Behaviour is triggered by calling *OnTriggerColor()*

### DirectReferences
This section uses direct references between objects in order to ensure the game logic is performed. The *Player.cs* script is in charge of notifying all the relevant components of the damage event. This creates a reference to the HPBar, a seperate game object.

### SerializedReferences/Core
This section uses the structure of *GameEvent*, *GameEventListener* and *ScriptableObject* explained in the Unity article, and expands on the structure by adding more features such as:

#### Scriptable Variable Generalization
*SerializedReferenceBase* was created to allow some common elements to *Scriptable Variables*. This include:
1. Giving a friendly name to the variable
2. Linking a ValueChanged *GameEvent* which is triggered every time the variable changes.
3. Give a basic ToString() implementation

*SerializedReference* was created as a template to encapsulate all usual logic handle in *Scriptable Variables*. This allows to create a new class inheriting from this to have all the required behaviours of a *Serialized Variable* of any serializable type with minimal effort.

#### Game Event Listeners Runtime
The basic use case of GameEventListeners made it so we had to add it to a GameObject from the editor window. There are some cases however where we would want to inject the *GameEvent* into the listener to modify the trigger at runtime (displayed in the *ValueDebugPresenterLine.cs*)

1. Possibility to set *GameEventListeners* in code with the *GameEventListener::SetEvent(GameEvent)* method.

### SerializedReferences/Gameplay
Here you will find the adapted scripts using the Event Structure in order to have to achieve the required game logic. Components do not hold references to each other anymore, and *GameEventListeners* are used in parallel with *SerializedReferences* to allow all the logic to work seemlessly.

### SerializedReferences/UI
Here you will find a very simple Debug Canvas which allows tracking *SerializedReferenceBases* in a game window. This allows updating the Window both by polling, or by signaling. This was made to show the strength of *Scriptable Variables*, as this whole UI behaviour is completely detached from our Scene Logic, and may be used wherever we see fit.

## Special Thanks and Credit
1. [Three ways to architect your game with ScriptableObjects](https://unity.com/how-to/architect-game-code-scriptable-objects) for providing the basic code for the GameEvent/Listener/Variable logic.
2. [ReadOnlyAttribute](https://gist.github.com/LotteMakesStuff/c0a3b404524be57574ffa5f8270268ea#gistcomment-2731505) @ZiadJ for providing this fantastic piece of code.