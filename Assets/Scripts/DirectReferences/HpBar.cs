﻿using UnityEngine;
using UnityEngine.UI;

public class HpBar : MonoBehaviour
{
    [SerializeField] Image FillBarRefence = default;

    public void UpdateBar(int currentHp, int maxHp)
    {
        FillBarRefence.fillAmount = (float)currentHp / (float)maxHp;
    }
}

