﻿using UnityEngine;

public class SoundPlayer : MonoBehaviour
{
    [SerializeField] AudioSource source = default;
    [SerializeField] AudioClip damageClip = default;

    public void OnPlayerDamageTaken()
    {
        source.PlayOneShot(damageClip);
    }
}

