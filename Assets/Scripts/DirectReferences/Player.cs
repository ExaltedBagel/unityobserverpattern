﻿using UnityEngine;
using Xltd.Visuals;

public class Player : MonoBehaviour
{  
    [Header("Object References")]
    [SerializeField] RendererColorInterpolator ColorInterpolator = default;
    [SerializeField] SoundPlayer damageSoundPlayer = default;

    [Header("External Object References")]
    [SerializeField] HpBar hpBar = default;

    [Header("Player Stats")]
    public int playerMaxHp;
    public int playerCurrentHp;

    public void TakeDamage(int damage)
    {
        playerCurrentHp -= damage;

        // Notify objects which are relevant
        ColorInterpolator.OnTriggerColor();
        hpBar.UpdateBar(playerCurrentHp, playerMaxHp);
        damageSoundPlayer.OnPlayerDamageTaken();
        
        if(playerCurrentHp <=0)
        {
            Destroy(gameObject);
        }
    }
}
