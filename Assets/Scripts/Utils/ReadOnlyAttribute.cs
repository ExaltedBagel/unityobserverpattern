﻿// *********************************************************************************************
// Code taken from
// https://gist.github.com/LotteMakesStuff/c0a3b404524be57574ffa5f8270268ea#gistcomment-2731505
// *********************************************************************************************

using System;
using UnityEngine;

namespace Xltd.Utils
{
[AttributeUsage(AttributeTargets.Field, Inherited = true)]
public class ReadOnlyAttribute : PropertyAttribute { }

#if UNITY_EDITOR
[UnityEditor.CustomPropertyDrawer(typeof(ReadOnlyAttribute))]
public class ReadOnlyAttributeDrawer : UnityEditor.PropertyDrawer
{
    public override void OnGUI(Rect rect, UnityEditor.SerializedProperty prop, GUIContent label)
    {
        bool wasEnabled = GUI.enabled;
        GUI.enabled = false;
        UnityEditor.EditorGUI.PropertyField(rect, prop);
        GUI.enabled = wasEnabled;
    }
}
#endif
}