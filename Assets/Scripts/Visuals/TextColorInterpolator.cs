﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Xltd.Visuals
{
    sealed class TextColorInterpolator : AbstractColorInterpolator
    {
        [Header("Object References")]
        [SerializeField] TMP_Text textObject = default;

        protected override void ApplyColor(Color newColor)
        {
            textObject.color = newColor;
        }

        protected override void EvaluateInitialColor()
        {
            InitialColor = textObject.color;
        }
    }
}