﻿using System.Collections;
using UnityEngine;

namespace Xltd.Visuals
{
    public abstract class AbstractColorInterpolator : MonoBehaviour
    {
        [Header("Color Settings")]
        [SerializeField] Color highlightColor = default;
        [SerializeField] float lerpTimeRamp = default;
        [SerializeField] float lerpTimeFade = default;

        protected Color InitialColor { get; set; }
        protected Coroutine ColorCoroutine { get; set; } = null;

        protected abstract void EvaluateInitialColor();
        protected abstract void ApplyColor(Color newColor);

        void Awake()
        {
            EvaluateInitialColor();
        }

        public void OnTriggerColor()
        {
            if (ColorCoroutine != null)
            {
                StopCoroutine(ColorCoroutine);
            }
            ColorCoroutine = StartCoroutine(InterpolateColorCoroutine());
        }

        IEnumerator InterpolateColorCoroutine()
        {
            float totalDuration = 0f;

            while (totalDuration < lerpTimeRamp)
            {
                totalDuration += Time.deltaTime;
                ApplyColor(Color.Lerp(InitialColor, highlightColor, totalDuration / lerpTimeRamp));
                yield return null;
            }

            totalDuration = 0f;
            while (totalDuration < lerpTimeFade)
            {
                totalDuration += Time.deltaTime;
                ApplyColor(Color.Lerp(highlightColor, InitialColor, totalDuration / lerpTimeFade));
                yield return null;
            }
        }
    }
}