﻿using UnityEngine;

namespace Xltd.Visuals
{
    sealed class RendererColorInterpolator : AbstractColorInterpolator
    {
        [Header("Object References")]
        [SerializeField] Renderer rend = default;

        protected override void ApplyColor(Color newColor)
        {
            rend.material.color = newColor;
        }

        protected override void EvaluateInitialColor()
        {
            InitialColor = rend.material.color;
        }
    }
}