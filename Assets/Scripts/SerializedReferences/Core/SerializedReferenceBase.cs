﻿using UnityEngine;

namespace Xltd.SerializedRef
{
    public abstract class SerializedReferenceBase : ScriptableObject
    {
        [Header("Debug Utility")]
        public string friendlyName;
        public abstract object RawValue { get; }

        [Header("Events")]
        public GameEvent valueChangedEvent = default;
        [SerializeField] protected bool alwaysRaise = default;

        public override string ToString()
        {
            return $"{(friendlyName != "" ? friendlyName : name)}: {RawValue}";
        }
    }
}
