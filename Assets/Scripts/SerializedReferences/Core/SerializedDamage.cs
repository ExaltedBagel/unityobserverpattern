﻿using UnityEngine;

namespace Xltd.SerializedRef
{
    [CreateAssetMenu(fileName = "Var_Damage_New", menuName = "Observer/SerializedDamage")]
    public class SerializedDamage : SerializedReference<Damage>
    {
        public GameObject Source => RuntimeValue.source;
        public int Amount => RuntimeValue.amount;
    }
}
