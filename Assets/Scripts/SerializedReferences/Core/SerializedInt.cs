﻿using UnityEngine;

namespace Xltd.SerializedRef
{
    [CreateAssetMenu(fileName = "Var_Int_New", menuName = "Observer/SerializedInt")]
    public class SerializedInt : SerializedReference<int>
    {

    }
}
