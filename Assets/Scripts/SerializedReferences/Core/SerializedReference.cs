﻿using UnityEngine;
using Xltd.Utils;

namespace Xltd.SerializedRef
{
	public abstract class SerializedReference<T> : SerializedReferenceBase, ISerializationCallbackReceiver
	{
		[Header("Values")]
		[SerializeField] T InitialValue = default;

		[SerializeField][ReadOnly] T _runtimeValue;
		public T RuntimeValue
		{
			get 
			{ 
				return _runtimeValue; 
			}
			set
			{
				if (alwaysRaise || _runtimeValue == null || !_runtimeValue.Equals(value))
				{
					_runtimeValue = value;
					valueChangedEvent?.Raise();
				}
			}
		}

		public override object RawValue => RuntimeValue;
		public virtual void OnAfterDeserialize() { _runtimeValue = InitialValue; }
		public virtual void OnBeforeSerialize() { }
	}
}
