﻿using System.Collections.Generic;
using UnityEngine;

namespace Xltd.SerializedRef
{
	[CreateAssetMenu(fileName = "NewGameEvent", menuName = "Observer/GameEvent")]
	public class GameEvent : ScriptableObject
	{
		List<GameEventListener> listeners = new List<GameEventListener>();

		public void Raise()
		{
			for (int i = listeners.Count - 1; i >= 0; i--)
			{
				if(listeners[i] == null)
				{
					listeners.RemoveAt(i);
				}
				else
				{
					listeners[i].OnEventRaised();
				}
			}
		}

		public void RegisterListener(GameEventListener listener)
		{
			if (!listeners.Contains(listener))
			{
				listeners.Add(listener);
			}
		}

		public void UnregisterListener(GameEventListener listener)
		{
			listeners.Remove(listener);
		}
	}
}
