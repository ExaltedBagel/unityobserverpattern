﻿using UnityEngine;
using UnityEngine.Events;

namespace Xltd.SerializedRef
{
    public class GameEventListener : MonoBehaviour
    {
        [SerializeField] GameEvent Event = default;
        [SerializeField] UnityEvent Response = default;

        private void OnEnable()
        {
            Event?.RegisterListener(this);
        }

        private void OnDisable()
        {
            Event?.UnregisterListener(this);
        }

        public void SetEvent(GameEvent gameEvent)
        {
            Event?.UnregisterListener(this);
            Event = gameEvent;
            Event?.RegisterListener(this);
        }

        public void OnEventRaised()
        {
            Response.Invoke();
        }
    }
}