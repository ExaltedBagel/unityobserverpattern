﻿using UnityEngine;
using UnityEngine.UI;
using Xltd.SerializedRef;

public class HpBarBetter : MonoBehaviour
{
    [Header("Scriptable References")]
    [SerializeField] SerializedInt CurrentHpRef = default;
    [SerializeField] SerializedInt MaxHpRef = default;

    [Header("Object References")]
    [SerializeField] Image FillBarRefence = default;

    public void UpdateBar()
    {
        FillBarRefence.fillAmount = (float)CurrentHpRef.RuntimeValue / (float)MaxHpRef.RuntimeValue;
    }
}

