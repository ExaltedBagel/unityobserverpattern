﻿using UnityEngine;
using Xltd.SerializedRef;

public class PlayerBetter : MonoBehaviour
{
    [Header("Scriptable References")]
    [SerializeField] SerializedInt CurrentHpRef = default;
    [SerializeField] SerializedInt MaxHpRef = default;

    [SerializeField] SerializedDamage LastDamageTaken = default;

    [Header("Raisable Events")]
    [SerializeField] GameEvent PlayerDied = default;

    public void Start()
    {
        CurrentHpRef.RuntimeValue = MaxHpRef.RuntimeValue;
    }

    public void OnDamageTaken()
    {
        CurrentHpRef.RuntimeValue -= LastDamageTaken.RuntimeValue.amount;

        if(CurrentHpRef.RuntimeValue <= 0)
        {
            PlayerDied?.Raise();
        }
    }

    public void OnPlayerDied()
    {
        Destroy(gameObject);
    }
}
