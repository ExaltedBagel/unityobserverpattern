﻿using UnityEngine;
using Xltd.SerializedRef;

public class DamageSource : MonoBehaviour
{
    [Header("Damager Data")]
    [SerializeField] SerializedDamage EnemyAttackData = default;

    [Header("Serialized References")]
    [SerializeField] SerializedDamage PlayerDamageRef = default;

    public void DamagePlayer()
    {
        PlayerDamageRef.RuntimeValue = new Damage { amount = EnemyAttackData.Amount, source = gameObject };
    }
}

