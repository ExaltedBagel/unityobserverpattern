﻿using System;
using UnityEngine;

[Serializable]
public struct Damage
{
    public GameObject source;
    public int amount;

    public override string ToString()
    {
        return $"{(source != null ? source.name : "Unknown")} -> {amount}";
    }
}

