﻿using System.Collections.Generic;
using UnityEngine;

namespace Xltd.SerializedRef.UI
{
    public class ValueDebugWindow : MonoBehaviour
    {
        [Header("Serialized References")]
        [SerializeField] List<SerializedReferenceBase> trackedValues = default;

        [Header("Debug Window Settings")]
        [SerializeField] bool isPolling = default;
        [SerializeField] ValueDebugPresenterLine objectPrefab = default;

        [Header("Object References")]
        [SerializeField] RectTransform contentRoot = default;
        List<ValueDebugPresenterLine> TextInstances { get; set; }

        bool lastPollingValue;

        void Awake()
        {
            lastPollingValue = isPolling;
            TextInstances = new List<ValueDebugPresenterLine>(trackedValues.Count);
            for (int i = 0; i < trackedValues.Count; i++)
            {
                TextInstances.Add(Instantiate(objectPrefab, contentRoot));
                TextInstances[i].gameObject.name = trackedValues[i].name;
                TextInstances[i].isPolling = isPolling;
                TextInstances[i].SetTrackedVariable(trackedValues[i]);
            }
        }

        void Update()
        {
            if(lastPollingValue != isPolling)
            {
                lastPollingValue = isPolling;
                foreach (var item in TextInstances)
                {
                    item.isPolling = lastPollingValue;
                }
            }
        }
    }
}
