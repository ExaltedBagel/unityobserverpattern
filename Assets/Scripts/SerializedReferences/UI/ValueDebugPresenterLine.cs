﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Xltd.SerializedRef.UI
{
    public class ValueDebugPresenterLine : MonoBehaviour
    {
        [Header("Behaviour")]
        public bool isPolling;

        [Header("Object References")]
        [SerializeField] TMP_Text TextObject = default;
        [SerializeField] GameEventListener gameEventListener = default;

        SerializedReferenceBase TrackedVariable = default;
        bool lastPollingValue;

        void Awake()
        {
            lastPollingValue = isPolling;
        }

        void Start()
        {
            gameEventListener.enabled = !isPolling;
            UpdateText();
        }

        void Update()
        {
            if(lastPollingValue != isPolling)
            {
                lastPollingValue = isPolling;
                gameEventListener.enabled = !isPolling;
            }

            if (isPolling)
            {
                UpdateText();
            }
        }

        public void SetTrackedVariable(SerializedReferenceBase variable)
        {
            TrackedVariable = variable;
            gameEventListener.SetEvent(variable.valueChangedEvent);
        }

        public void UpdateText()
        {
            TextObject.text = TrackedVariable.ToString();
        }
    }
}
